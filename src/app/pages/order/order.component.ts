import { Component, OnInit } from '@angular/core';
import { IOrder } from 'src/app/models/order';
import {OrderService} from "../../services/order/order.service";
import {UserService} from "../../services/user/user.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  orders: any;

  constructor( private orderService:OrderService,
               private userService: UserService) { }

  ngOnInit(): void {
    const userId =<string>this.userService.getUser()?.id;
    this.orderService.getOrders(userId).subscribe((data)=>{
      this.orders = data;
      // console.log(data);
    });

  }

}
